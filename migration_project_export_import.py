#!/usr/bin/env python3
"""\
Export and Import a project from source(pilot) GitLab to new GitLab

Usage: migration_project_export_import.py

Author: Donghee Kang
Email : donghee.kang@kit.edu
"""
import gitlab
import os
import sys
import urllib3
import json
import time
from tqdm import tqdm

"""
Disable ssl warning: suppresses ssl warning for disabling certificate check
"""
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

##########################################################################################################
# Credential
########################################################################################################## 
KIT_PILOT_URL = "https://git.scc.kit.edu"
KIT_GITLAB_URL = "https://gitlab.kit.edu"

KIT_PILOT_TOKEN = "----------------------------------"  # Personal Access Token, please change with yours
KIT_GITLAB_TOKEN = "----------------------------------" # Personal Access Token, please change with yours

gl_kit = gitlab.Gitlab(KIT_GITLAB_URL, private_token=KIT_GITLAB_TOKEN, ssl_verify=False, api_version="4")
gl_kit.auth()

gl_pilot = gitlab.Gitlab(KIT_PILOT_URL, private_token=KIT_PILOT_TOKEN, ssl_verify=False, api_version="4")
gl_pilot.auth()

##########################################################################################################
# Parameter   
##########################################################################################################
project_id_in_source = None                      # (Optional) None or 123456789 

source_namespace = "xx1234/migration_project"    # user/project namespace in source gitlab

target_namespace = "firstname.lastname"          # user namespace in target gitlab

##########################################################################################################
# Migration
##########################################################################################################
class migration():

    def export_import_project(self):
        
        # parameter: project id has a higher priority 
        if project_id_in_source:
            project_access = project_id_in_source
        elif source_namespace:
            project_access = source_namespace
        else: 
            print("Please provide project_id or project_namespace from the source GtiLab")
        
        # export
        project = gl_pilot.projects.get(project_access)
        project_export = project.exports.create()

        # replace spaces with dash
        project_name = project.name.replace(' ', '-')

        # Wait for the 'finished' status of export, but python-gitlab version dependent!
        #while project_export.export_status != 'finished':
        #    time.sleep(10)
        #    project_export.refresh()

        # wait for the preparation shortly
        time.sleep(3)
        print("Exporting process for project {} from the source GitLab start".format(project_name))

        # Wait 10sec(small project) or 100sec(big project)
        with tqdm(total=100, desc="Exporting {}".format(project_name), bar_format="{l_bar}{bar} [ time left: {remaining} ]") as pbar:
            for i in range(100):
                time.sleep(0.1)   # 0.1 -> 10sec or 1 -> 100sec
                pbar.update(1)

        current_working_directory = os.getcwd()
        exported_file_directory = current_working_directory + "/exported_files"
        file_is = '{}/{}_export.tar.gz'.format(exported_file_directory, project_name)

        # download
        print("Downloading project {} from the source GitLab, please wait while we download project ".format(project_name))
        with open(file_is, 'wb') as f:
            project_export.download(streamed=True, action=f.write)

        # importing
        with open(file_is, 'rb') as f:
            output = gl_kit.projects.import_project(
                f, 
                path=project_name,
                name=project.name.capitalize(),
                namespace=target_namespace,
                override_params={'visibility': 'private'},
            )
        
        # Get a ProjectImport object to track the import status
        project_import = gl_kit.projects.get(output['id'], lazy=True).imports.get()
        while project_import.import_status != 'finished':
            # Wait 10sec(small project) or 100sec(big project)
            with tqdm(total=100, desc="Importing {}".format(project_name), bar_format="{l_bar}{bar} [ time left: {remaining} ]") as pbar:
                for i in range(100):
                    time.sleep(0.1)   # 0.1 -> 10sec or 1 -> 100sec
                    pbar.update(1)
            project_import.refresh()

        print("Importing project {} to the target GitLab is completed ".format(project_name))

if __name__ == "__main__":

    # let's start 
    migration_accessor = migration()
    migration_accessor.export_import_project()
