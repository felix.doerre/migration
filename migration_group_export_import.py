#!/usr/bin/env python3
"""\
Exprot and Import a group from source(pilot) GitLab to new GitLab

Usage: migration_group_export_import.py

Author: Donghee Kang
Email : donghee.kang@kit.edu
"""
import gitlab
import os
import sys
import urllib3
import json
import time
from tqdm import tqdm

"""
Disable ssl warning: suppresses ssl warning for disabling certificate check
"""
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

##########################################################################################################
# Credential
########################################################################################################## 
KIT_PILOT_URL = "https://git.scc.kit.edu"
KIT_GITLAB_URL = "https://gitlab.kit.edu"

KIT_PILOT_TOKEN = "---------------------------------"   # Personal Access Token, please change with yours
KIT_GITLAB_TOKEN = "----------------------------------" # Personal Access Token, please change with yours

gl_kit = gitlab.Gitlab(KIT_GITLAB_URL, private_token=KIT_GITLAB_TOKEN, ssl_verify=False, api_version="4")
gl_pilot = gitlab.Gitlab(KIT_PILOT_URL, private_token=KIT_PILOT_TOKEN, ssl_verify=False, api_version="4")

##########################################################################################################
# Parameter 
##########################################################################################################
# source group id
group_id_in_source = 19277          # required parameter

# In target gitlab, group path has to define. "KIT / Institute" must be existing.
# If the last subgroup "Migration" is not existing, 
#          "Migration" will be created first and projects wil be transferred on this subgroup. 
# If the last subgroup "Migration" is already existing, 
#          then projects will be transferred to existing subgroup, directly. 
group_namespace_in_target = 'kit/institute/migration' 

##########################################################################################################
# Migration
##########################################################################################################
class migration():

    def export_projects(self):
        # To perform a new export, a group id must to be given.   
        if group_id_in_source is None:
            print("Group ID is not provided, please find the group id from the source gitlab")
            exit()

        gl_pilot.auth()
        current_working_directory = os.getcwd()
        exported_file_directory = current_working_directory + "/exported_files"

        group = gl_pilot.groups.get(group_id_in_source, lazy=True)
        projects = group.projects.list(get_all=True, iterator=True)
        for project in (project for project in projects if len(projects) > 0):

            project_name = project.name.replace(' ', '-')

            # Get the project object using project_id, project itself does not have exports function.
            single_project = gl_pilot.projects.get(project.id)
            project_export = single_project.exports.create()

            # Wait for the 'finished' status of export, but python-gitlab version dependent!
            #while project_export.export_status != 'finished':
            #    time.sleep(10)
            #    project_export.refresh()

            # wait for the preparation shortly
            time.sleep(3)
            print("Exporting process for project {} from the source GitLab start".format(project_name))
           
            # Wait 10sec(small project) or 100sec(big project)
            with tqdm(total=100, desc="Exporting {}".format(project_name), bar_format="{l_bar}{bar} [ time left: {remaining} ]") as pbar:
                for i in range(100):
                    time.sleep(0.1)   # 0.1 -> 10sec or 1 -> 100sec
                    pbar.update(1)  
            project_export.refresh()

            # Download the result
            print("Downloading project {} from the source GitLab, please wait while we download project ".format(project_name))
            file_is = '{}/{}_export.tar.gz'.format(exported_file_directory, project_name)
            with open(file_is, 'wb') as f:
                project_export.download(streamed=True, action=f.write)

    def import_projects(self):

        gl_kit.auth()
        current_working_directory = os.getcwd()
        exported_file_directory = current_working_directory + "/exported_files"

        # create last subgroup if it is not existing
        structure_of_group = group_namespace_in_target.split('/')
        subgroup_id = None
        layer = 0
        for group_name in structure_of_group:
            layer += 1

            if layer == 1:
                group = gl_kit.groups.get(group_name.upper())
            else:
                group = gl_kit.groups.get(subgroup_id)

            parent_group_id = subgroup_id

            subgroups = group.subgroups.list(get_all=True, iterator=True)
            for subgroup in subgroups:
                subgroup_id = None
                if subgroup.attributes.get('path') == structure_of_group[layer]:
                    subgroup_id = subgroup.attributes.get('id')
                    break
            
            # create last subgroup, and provide its id
            if (layer == (len(structure_of_group)-1)) and (not subgroup_id):
                new_subgroup = gl_kit.groups.create(
                    {
                        'name': structure_of_group[layer].capitalize(), 
                        'path': structure_of_group[layer], 
                        'parent_id': parent_group_id
                    }
                )
                new_subgroup.save()
                time.sleep(3)
                subgroup_id = new_subgroup.id
                print("New subgroup is created on the path : {}".format(group_namespace_in_target))

        # prepare the list of exported files under exported_file_directory
        list_of_projects = []
        list_of_files = os.listdir(exported_file_directory)
        list_of_files.sort()
        for project in list_of_files:
            name_of_project = project.removesuffix("_export.tar.gz")
            #name_of_project = project.strip("_export.tar.gz")      # thjs does not work for a_export.tar.gz
            list_of_projects.append(name_of_project)

        # Importing 
        for project_name in list_of_projects:
            file_is = "{}/{}_export.tar.gz".format(exported_file_directory, project_name)
            with open(file_is, 'rb') as f:
                output = gl_kit.projects.import_project(
                    f, 
                    path=project_name, 
                    name=project_name.replace('-', ' ').capitalize(),
                    namespace=group_namespace_in_target,
                    override_params={'visibility': 'private'},
                )

            # Get a ProjectImport object to track the import status
            project_import = gl_kit.projects.get(output['id'], lazy=True).imports.get()
            while project_import.import_status != 'finished':
                # Wait 10sec(small project) or 100sec(big project)
                with tqdm(total=100, desc="Importing {}".format(project_name), bar_format="{l_bar}{bar} [ time left: {remaining} ]") as pbar:
                    for i in range(100):
                        time.sleep(0.1)   # 0.1 -> 10sec or 1 -> 100sec
                        pbar.update(1)  
                project_import.refresh()

        print("Exporting and Importing group is completed. Please take a look transferred projects on your group")
        print("https://gitlab.kit.edu/{}".format(group_namespace_in_target))

if __name__ == "__main__":

    # let's start 
    migration_accessor = migration()
    migration_accessor.export_projects()
    migration_accessor.import_projects()
