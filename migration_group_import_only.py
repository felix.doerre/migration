#!/usr/bin/env python3
"""\
Import a group from source(pilot) GitLab to new GitLab

Usage: migration_group_import_only.py

Author: Donghee Kang
Email : donghee.kang@kit.edu
"""

import gitlab
import os
import sys
import urllib3
import json
import time
from tqdm import tqdm

"""
Disable ssl warning: suppresses ssl warning for disabling certificate check
"""
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

##########################################################################################################
# Credential
########################################################################################################## 
KIT_GITLAB_URL = "https://gitlab.kit.edu"

KIT_TOKEN = "---------------------------------"   # Personal Access Token, please change with yours

gl = gitlab.Gitlab(KIT_GITLAB_URL, private_token=KIT_TOKEN, ssl_verify=False, api_version="4")
gl.auth()

##########################################################################################################
# Parameter 
########################################################################################################## 
# Source: Exported files are available in your local {PWD}/exported_file directory. 
list_of_projects = ["my_project_1", "my_project_2", "my_project_3"]

# In target gitlab, group path has to define. "KIT / Institute" must be existing.
# If the last subgroup "Migration" is not existing, 
#          "Migration" will be created first and projects wil be transferred on this subgroup. 
# If the last subgroup "Migration" is already existing, 
#          then projects will be transferred to existing subgroup, directly. 
group_namespace_in_target = 'kit/institute/migration' 

##########################################################################################################
# Migration
##########################################################################################################
class migration():

    def import_projects(self):

        current_working_directory = os.getcwd()
        exported_file_directory = current_working_directory + "/exported_files"

        # create last subgroup if it is not existing
        structure_of_group = group_namespace_in_target.split('/')
        subgroup_id = None
        layer = 0
        for group_name in structure_of_group:
            layer += 1

            if layer == 1:
                group = gl.groups.get(group_name.upper())
            else:
                group = gl.groups.get(subgroup_id)

            parent_group_id = subgroup_id

            subgroups = group.subgroups.list(get_all=True, iterator=True)
            for subgroup in subgroups:
                subgroup_id = None
                if subgroup.attributes.get('path') == structure_of_group[layer]:
                    subgroup_id = subgroup.attributes.get('id')
                    break
            
            # create last subgroup, and provide its id
            if (layer == (len(structure_of_group)-1)) and (not subgroup_id):
                new_subgroup = gl.groups.create(
                    {
                        'name': structure_of_group[layer].capitalize(), 
                        'path': structure_of_group[layer], 
                        'parent_id': parent_group_id
                    }
                )
                new_subgroup.save()
                time.sleep(3)
                subgroup_id = new_subgroup.id
                print("New subgroup is created on the path : {}".format(group_namespace_in_target))

        for project_name in list_of_projects:

            file_is = "{}/{}_export.tar.gz".format(exported_file_directory, project_name)
            with open(file_is, 'rb') as f:
                output = gl.projects.import_project(
                    f,
                    path=project_name,
                    name=project_name.capitalize(),
                    namespace=group_namespace_in_target,
                    override_params={'visibility': 'private'},
                )

            # wait for the preparation shortly
            time.sleep(3)
            print("Start importing process for project {} on target GitLab ".format(project_name))

            # Get a ProjectImport object to track the import status
            project_import = gl.projects.get(output['id'], lazy=True).imports.get()
            while project_import.import_status != 'finished':
                # Wait 10sec(small project) or 100sec(big project)
                with tqdm(total=100, desc="Importing {}".format(project_name), bar_format="{l_bar}{bar} [ time left: {remaining} ]") as pbar:
                    for i in range(100):
                        time.sleep(0.1)   # 0.1 -> 10sec or 1 -> 100sec
                        pbar.update(1)
                project_import.refresh()

        print("Importing projects into the group is completed. Please take a look transferred projects on your group")
        print("https://gitlab.kit.edu/{}".format(group_namespace_in_target))

if __name__ == "__main__":

    # let's start collecting project information
    migration_accessor = migration()
    migration_accessor.import_projects()
